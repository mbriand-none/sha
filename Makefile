CFLAGS = -Wall -Wextra -pedantic -Werror

all: sha symlinks

.PHONY: clean bintests

#
# Common
#
HEADERS = $(wildcard src/*.h src/tests/*.h)

out/%.o: src/%.c $(HEADERS)
	mkdir -p $(dir $@)
	gcc $(CFLAGS) -c $< -o $@

clean:
	rm -rf out
	rm -rf cmocka


# Binary

SRC = src/main.c \
      src/input.c \
      src/sha1.c \
      src/sha2.c \

OBJ = $(patsubst src/%.c,out/%.o,$(SRC))

sha: $(OBJ)
	gcc $(LDFLAGS) -o $@ $^

symlinks: sha
	ln -sf sha sha224
	ln -sf sha sha256
	ln -sf sha sha384
	ln -sf sha sha512
	ln -sf sha sha1

#
# CMocka tests
#

CMOCKA_SRC = $(wildcard src/tests/*.c)
CMOCKA_OBJ = $(patsubst src/%.c,out/%.o,$(CMOCKA_SRC))
CMOCKA_BIN = $(patsubst src/tests/%.c,cmocka/%,$(CMOCKA_SRC))
CMOCKA_XMLS = $(patsubst src/tests/%.c,tests_results/%.xml,$(CMOCKA_SRC))

CFLAGS = -Wall -Wextra -pedantic
ifeq ($(DEBUG),1)
CFLAGS += -g
else
CFLAGS += -Werror
endif

out/tests/%.o: src/tests/%.c $(HEADERS) $(SRC)
	mkdir -p $(dir $@)
	gcc $(CFLAGS) -c $< -o $@

cmocka/%: out/tests/%.o
	mkdir -p $(dir $@)
	gcc $(LDFLAGS) -o $@ $^ -lcmocka

cmocka_tests: $(CMOCKA_BIN)

run_cmocka_tests: $(CMOCKA_BIN)
	for bin in $^; do ./$$bin; done

tests_results/%.xml: cmocka/%
	mkdir -p $(dir $@)
	./$< --xml > $@

run_cmocka_tests_xml: $(CMOCKA_XMLS)

#
# Binary tests
#

bintests:
	make -C bintests

bintests_stats:
	make -C bintests $@

#
# Callgrind
#

CALLGRIND_TEST_FILE=out/tests/testfile_random_10M
callgrind.out: symlinks $(CALLGRIND_TEST_FILE)
	valgrind --tool=callgrind --callgrind-out-file=$@ ./sha256 $(CALLGRIND_TEST_FILE)

callgrind: callgrind.out
	callgrind_annotate $<

