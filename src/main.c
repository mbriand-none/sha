#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sha1.h"
#include "sha2.h"

static void print_hash(const char *file, const unsigned char *sum,
		       size_t len)
{
	size_t i;

	for (i = 0; i < len; ++i)
		printf("%02x", sum[i]);

	printf(" %s\n", file);
}

static size_t sha_len[] = {
	[SHA1] = SHA1_LEN,
	[SHA224] = SHA224_LEN,
	[SHA256] = SHA256_LEN,
	[SHA384] = SHA384_LEN,
	[SHA512] = SHA512_LEN,
};

static int do_sha(enum sha_alg alg, const char **files, unsigned int cpt)
{
	unsigned int i;
	unsigned char sum[sha_len[alg]];

	for (i = 0; i < cpt; ++i) {
		int fd;
		int ret;

		fd = open(files[i], 'r');
		if (fd < 0) {
			fprintf(stderr, "Failed to open '%s': %s\n", files[i],
				strerror(errno));
			continue;
		}

		switch (alg) {
		case SHA1:
			ret = sha1_fd(fd, sum);
			break;

		case SHA224:
		case SHA256:
		case SHA384:
		case SHA512:
			ret = sha2_fd(alg, fd, sum);
			break;

		default:
			return -EINVAL;
		}

		if (ret < 0) {
			fprintf(stderr, "Failed to hash '%s' (%d)\n", files[i],
				ret);
			close(fd);
			continue;
		}

		close(fd);
		
		print_hash(files[i], sum, sha_len[alg]);
	}

	return EXIT_SUCCESS;
}

int main(int argc, const char *argv[])
{
	const char **files = argv + 1;
	unsigned int filecnt = argc - 1;
	const char *cmd = NULL;

	/* Quick and dirty basename implementation.
	 */
	cmd = strrchr(argv[0], '/');
	if (cmd != NULL)
		cmd++;
	else
		cmd = argv[0];

	if (!strcmp(cmd, "sha224")) {
		return do_sha(SHA224, files, filecnt);
	} else if (!strcmp(cmd, "sha256")) {
		return do_sha(SHA256, files, filecnt);
	} else if (!strcmp(cmd, "sha384")) {
		return do_sha(SHA384, files, filecnt);
	} else if (!strcmp(cmd, "sha512")) {
		return do_sha(SHA512, files, filecnt);
	} else if (!strcmp(cmd, "sha1")) {
		return do_sha(SHA1, files, filecnt);
	} else {
		fprintf(stderr, "Invalid target: %s\n", cmd);
		return EXIT_FAILURE;
	}
}
