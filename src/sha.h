#ifndef __SHA_H__
#define __SHA_H__

enum sha_alg {
	SHA1,
	SHA224,
	SHA256,
	SHA384,
	SHA512
};

#endif
