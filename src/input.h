#ifndef __INPUT_H__
#define __INPUT_H__

#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>

struct get_block_ctx {
	size_t read_len;

	/* The last block, containing the size, has been generated.
	 */
	bool last_block;

	/* Specific data */
};


struct fdinput_ctx {
	int fd;
};

struct bufinput_ctx {
	const uint8_t *start;
	const uint8_t *end;
};


void fdinput_init(struct fdinput_ctx *ctx, int fd);
int fdinput_read(unsigned char *buf, size_t len, void *ctx);

void bufinput_init(struct bufinput_ctx *ctx, const uint8_t *buf, size_t len);
int bufinput_read(unsigned char *buf, size_t len, void *ctx);

void get_block_init(struct get_block_ctx *ctx);
int get_block(unsigned char *buf, size_t len, struct get_block_ctx *ctx,
	      int (*input_read)(unsigned char *, size_t, void *),
	      void *input_ctx);

#endif
