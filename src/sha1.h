#ifndef __SHA1_H__
#define __SHA1_H__

#include <stdint.h>
#include <unistd.h>

#define SHA1_LEN	(160 / 8)

int sha1_buf(const uint8_t *buf, size_t len, unsigned char *sum);
int sha1_fd(const int fd, unsigned char *sum);

#endif
