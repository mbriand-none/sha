#include <endian.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include "input.h"

void fdinput_init(struct fdinput_ctx *ctx, int fd)
{
	ctx->fd = fd;
}

int fdinput_read(unsigned char *buf, size_t len, void *ctx)
{
	int fd = ((struct fdinput_ctx *) ctx)->fd;
	size_t offset = 0;

	if (len == 0)
		return -EINVAL;

	while (offset < len) {
		ssize_t rlen;

		rlen = read(fd, buf + offset, len - offset);

		if (rlen < 0) {
			return rlen;
		} else if (rlen == 0) {
			break;
		}
		offset += rlen;
	}

	return offset;
}

void bufinput_init(struct bufinput_ctx *ctx, const uint8_t *buf, size_t len)
{
	ctx->start = buf;
	ctx->end = buf + len;
}

int bufinput_read(unsigned char *buf, size_t len, void *ctx_)
{
	struct bufinput_ctx *ctx = ctx_;
	size_t rlen = 0;

	if (ctx->start + len < ctx->end)
		rlen = len;
	else
		rlen = ctx->end - ctx->start;
	memcpy(buf, ctx->start, rlen);
	ctx->start += rlen;

	return rlen;
}

void get_block_init(struct get_block_ctx *ctx)
{
	memset(ctx, 0, sizeof(*ctx));
}

int get_block(unsigned char *buf, size_t len, struct get_block_ctx *ctx,
	      int (*input_read)(unsigned char *, size_t, void *),
	      void *input_ctx)
{
	size_t offset = 0;
	int rlen;

	rlen = input_read(buf, len, input_ctx);
	if (rlen < 0) {
		return rlen;
	}
	offset += rlen;
	ctx->read_len += rlen;

	if ((offset != len) && !ctx->last_block && (offset < len)) {
		/* First bit after data must be a 1.
		 */
		if ((ctx->read_len % len) == offset)
			buf[offset++] = 0x80;

		if ((len - offset) < sizeof(uint64_t)) {
			memset(buf + offset, 0, (len - offset));
			offset = len;
		} else {
			size_t padlen;
			uint64_t flen;

			padlen = len - offset - sizeof(uint64_t);
			memset(buf + offset, 0, padlen);
			offset += padlen;

			flen = htobe64(ctx->read_len * 8);
			memcpy(buf + offset, &flen, sizeof(uint64_t));
			offset += sizeof(uint64_t);

			ctx->last_block = true;
		}
	}

	return offset;
}
