#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdint.h>

static inline uint32_t lr32(uint32_t val, unsigned int offset)
{
	return (val << offset) | (val >> (32 - offset));
}

static inline uint32_t rr32(uint32_t val, unsigned int offset)
{
	return (val >> offset) | (val << (32 - offset));
}

static inline uint64_t rr64(uint64_t val, unsigned int offset)
{
	return (val >> offset) | (val << (64 - offset));
}


#endif
