#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmocka.h>
#include "../input.h"

#include "../input.c"
#include "../sha1.c"

/* sha1_buf()
 */

void test_sha1_buf(uint8_t *in, size_t inlen, const uint8_t *refsum)
{
	uint8_t sum[SHA1_LEN];
	int ret;

	ret = sha1_buf(in, inlen, sum);
	assert_int_equal(ret, 0);
	assert_memory_equal(sum, refsum, SHA1_LEN);
}

void test_sha1_len_0(void **state)
{
	const uint8_t refsum[SHA1_LEN] = {
		0xda, 0x39, 0xa3, 0xee, 0x5e, 0x6b, 0x4b, 0x0d,
		0x32, 0x55, 0xbf, 0xef, 0x95, 0x60, 0x18, 0x90,
		0xaf, 0xd8, 0x07, 0x09,
	};
	uint8_t *in = NULL;

	(void) state;
	test_sha1_buf(in, 0, refsum);
}

void test_sha1_len_4(void **state)
{
	const uint8_t refsum[SHA1_LEN] = {
		0xa9, 0x4a, 0x8f, 0xe5, 0xcc, 0xb1, 0x9b, 0xa6,
		0x1c, 0x4c, 0x08, 0x73, 0xd3, 0x91, 0xe9, 0x87,
		0x98, 0x2f, 0xbb, 0xd3,
	};
	uint8_t in[4] = {'t', 'e', 's', 't'};

	(void) state;
	test_sha1_buf(in, sizeof(in), refsum);
}

void test_sha1_len_128(void **state)
{
	const uint8_t refsum[SHA1_LEN] = {
		0xb5, 0xd3, 0x60, 0x71, 0xc0, 0x84, 0x5d, 0xf9,
		0xe0, 0x72, 0x7b, 0x58, 0xec, 0x97, 0xd2, 0x79,
		0x6f, 0x33, 0x12, 0xd0,
	};
	uint8_t in[128];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(in); ++i)
		in[i] = '0' + (i % 10);
	
	test_sha1_buf(in, sizeof(in), refsum);
}

/* sha256_extend() tests.
 */

static void sha1_extend_ref(uint32_t *w)
{
	unsigned int i;

	for (i = 16; i < 80; ++i) {
		w[i] = lr32(w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16], 1);
	}
}

void test_sha1_extend_zeros(void **state)
{
	uint32_t w[80];
	uint32_t wref[80];

	(void) state;

	memset(w, 0, sizeof(w));
	memset(wref, 0, sizeof(wref));

	sha1_extend_ref(wref);
	sha1_extend(w);

	assert_memory_equal(w, wref, sizeof(w));
}

void test_sha1_extend(void **state)
{
	uint32_t w[80];
	uint32_t wref[80];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(w) / sizeof(w[0]); ++i)
		wref[i] = w[i] = i | ((i ^ 0xFF) << 22);

	memset(w, 0, sizeof(w));
	memset(wref, 0, sizeof(wref));

	sha1_extend_ref(wref);
	sha1_extend(w);

	assert_memory_equal(w, wref, sizeof(w));
}


/* sha1_compress() tests.
 */

static void sha1_compress_ref(const uint32_t *w, uint32_t *vars)
{
	unsigned int i;

	for (i = 0; i < 80; ++i) {
		uint32_t f;
		uint32_t k;
		uint32_t temp;

		if (i < 20) {
			f = (vars[1] & vars[2]) | ((~vars[1]) & vars[3]);
			k = 0x5A827999;
		} else if (i < 40) {
			f = vars[1] ^ vars[2] ^ vars[3];
			k = 0x6ED9EBA1;
		} else if (i < 60) {
			f = (vars[1] & vars[2]) | (vars[1] & vars[3]) | (vars[2] & vars[3]);
			k = 0x8F1BBCDC;
		} else {
			f = vars[1] ^ vars[2] ^ vars[3];
			k = 0xCA62C1D6;
		}

		temp = lr32(vars[0], 5) + f + vars[4] + k + w[i];
		vars[4] = vars[3];
		vars[3] = vars[2];
		vars[2] = lr32(vars[1], 30);
		vars[1] = vars[0];
		vars[0] = temp;
	}
}

void test_sha1_compress_zeros(void **state)
{
	uint32_t varsref[5];
	uint32_t vars[5];
	uint32_t w[64];

	(void) state;

	memset(vars, 0, sizeof(vars));
	memset(varsref, 0, sizeof(varsref));
	memset(w, 0, sizeof(w));

	sha1_compress_ref(w, varsref);
	sha1_compress(w, vars);

	assert_memory_equal(vars, varsref, sizeof(vars));
}

void test_sha1_compress(void **state)
{
	uint32_t varsref[5];
	uint32_t vars[5];
	uint32_t w[64];
	uint32_t i;

	(void) state;

	for (i = 0; i < sizeof(vars) / sizeof(vars[0]); ++i)
		varsref[i] = vars[i] = (i ^ 0xFF) | (i << 22);
	
	for (i = 0; i < sizeof(w) / sizeof(w[0]); ++i)
		w[i] = i | ((i ^ 0xFF) << 22);

	sha1_compress_ref(w, varsref);
	sha1_compress(w, vars);

	assert_memory_equal(vars, varsref, sizeof(vars));
}

/* Component test.
 */

const struct CMUnitTest sha1_tests[] = {
	/* sha1() */
	cmocka_unit_test(test_sha1_len_0),
	cmocka_unit_test(test_sha1_len_4),
	cmocka_unit_test(test_sha1_len_128),
	/* sha1_extend() */
	cmocka_unit_test(test_sha1_extend_zeros),
	cmocka_unit_test(test_sha1_extend),
	/* sha1_compress() */
	cmocka_unit_test(test_sha1_compress_zeros),
	cmocka_unit_test(test_sha1_compress),
};

int main(int argc, char *argv[])
{
	if ((argc == 2) && (!strcmp(argv[1], "--xml"))) {
		cmocka_set_message_output(CM_OUTPUT_XML);
	} else if (argc != 1) {
		fprintf(stderr, "Usage: %s [--xml]\n", argv[0]);
		return 2;
	}

	cmocka_run_group_tests(sha1_tests, NULL, NULL);
	
	return EXIT_SUCCESS;
}

