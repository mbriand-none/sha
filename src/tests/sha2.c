#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmocka.h>
#include "../input.h"

#include "../input.c"
#include "../sha2.c"

/* sha*_buf()
 */

void test_sha_buf(enum sha_alg alg, size_t sumlen, uint8_t *in, size_t inlen,
		  const uint8_t *refsum)
{
	uint8_t *sum = malloc(sumlen);
	int ret;

	ret = sha2_buf(alg, in, inlen, sum);
	assert_int_equal(ret, 0);
	assert_memory_equal(sum, refsum, sumlen);

	free(sum);
}

void test_sha256_len_0(void **state)
{
	const uint8_t refsum[SHA256_LEN] = {
		0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14,
		0x9a, 0xfb, 0xf4, 0xc8, 0x99, 0x6f, 0xb9, 0x24,
		0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c,
		0xa4, 0x95, 0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55,
	};
	uint8_t *in = NULL;

	(void) state;
	test_sha_buf(SHA256, SHA256_LEN, in, 0, refsum);
}

void test_sha256_len_4(void **state)
{
	const uint8_t refsum[SHA256_LEN] = {
		0x9f, 0x86, 0xd0, 0x81, 0x88, 0x4c, 0x7d, 0x65,
		0x9a, 0x2f, 0xea, 0xa0, 0xc5, 0x5a, 0xd0, 0x15,
		0xa3, 0xbf, 0x4f, 0x1b, 0x2b, 0x0b, 0x82, 0x2c,
		0xd1, 0x5d, 0x6c, 0x15, 0xb0, 0xf0, 0x0a, 0x08,
	};
	uint8_t in[4] = {'t', 'e', 's', 't'};

	(void) state;
	test_sha_buf(SHA256, SHA256_LEN, in, sizeof(in), refsum);
}

void test_sha256_len_128(void **state)
{
	const uint8_t refsum[SHA256_LEN] = {
		0x91, 0x6e, 0x19, 0x99, 0x2e, 0x7f, 0x9b, 0x1e,
		0x8d, 0x92, 0x67, 0xc3, 0x32, 0x46, 0x16, 0xda,
		0xc8, 0xf4, 0x19, 0x94, 0x19, 0xc6, 0xe4, 0xeb,
		0xf6, 0x8f, 0xda, 0x98, 0x5f, 0x4b, 0x64, 0xea,
	};
	uint8_t in[128];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(in); ++i)
		in[i] = '0' + (i % 10);
	
	test_sha_buf(SHA256, SHA256_LEN, in, sizeof(in), refsum);
}

void test_sha224_len_0(void **state)
{
	const uint8_t refsum[SHA224_LEN] = {
		0xd1, 0x4a, 0x02, 0x8c, 0x2a, 0x3a, 0x2b, 0xc9,
		0x47, 0x61, 0x02, 0xbb, 0x28, 0x82, 0x34, 0xc4,
		0x15, 0xa2, 0xb0, 0x1f, 0x82, 0x8e, 0xa6, 0x2a,
		0xc5, 0xb3, 0xe4, 0x2f,
	};
	uint8_t *in = NULL;

	(void) state;
	test_sha_buf(SHA224, SHA224_LEN, in, 0, refsum);
}

void test_sha224_len_4(void **state)
{
	const uint8_t refsum[SHA224_LEN] = {
		0x90, 0xa3, 0xed, 0x9e, 0x32, 0xb2, 0xaa, 0xf4,
		0xc6, 0x1c, 0x41, 0x0e, 0xb9, 0x25, 0x42, 0x61,
		0x19, 0xe1, 0xa9, 0xdc, 0x53, 0xd4, 0x28, 0x6a,
		0xde, 0x99, 0xa8, 0x09,
	};
	uint8_t in[4] = {'t', 'e', 's', 't'};

	(void) state;
	test_sha_buf(SHA224, SHA224_LEN, in, sizeof(in), refsum);
}

void test_sha224_len_128(void **state)
{
	const uint8_t refsum[SHA224_LEN] = {
		0xe0, 0xa0, 0x44, 0x37, 0x76, 0xaa, 0x51, 0x75,
		0xf3, 0x24, 0xfd, 0xb8, 0x2a, 0xfe, 0x95, 0x81,
		0x53, 0x23, 0xd2, 0xa4, 0xa0, 0x3b, 0xb0, 0xd1,
		0x40, 0x8b, 0x8a, 0xba,
	};
	uint8_t in[128];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(in); ++i)
		in[i] = '0' + (i % 10);
	
	test_sha_buf(SHA224, SHA224_LEN, in, sizeof(in), refsum);
}

void test_sha384_len_0(void **state)
{
	const uint8_t refsum[SHA384_LEN] = {
		0x38, 0xb0, 0x60, 0xa7, 0x51, 0xac, 0x96, 0x38,
		0x4c, 0xd9, 0x32, 0x7e, 0xb1, 0xb1, 0xe3, 0x6a,
		0x21, 0xfd, 0xb7, 0x11, 0x14, 0xbe, 0x07, 0x43,
		0x4c, 0x0c, 0xc7, 0xbf, 0x63, 0xf6, 0xe1, 0xda,
		0x27, 0x4e, 0xde, 0xbf, 0xe7, 0x6f, 0x65, 0xfb,
		0xd5, 0x1a, 0xd2, 0xf1, 0x48, 0x98, 0xb9, 0x5b,
	};
	uint8_t *in = NULL;

	(void) state;
	test_sha_buf(SHA384, SHA384_LEN, in, 0, refsum);
}

void test_sha384_len_4(void **state)
{
	const uint8_t refsum[SHA384_LEN] = {
		0x76, 0x84, 0x12, 0x32, 0x0f, 0x7b, 0x0a, 0xa5,
		0x81, 0x2f, 0xce, 0x42, 0x8d, 0xc4, 0x70, 0x6b,
		0x3c, 0xae, 0x50, 0xe0, 0x2a, 0x64, 0xca, 0xa1,
		0x6a, 0x78, 0x22, 0x49, 0xbf, 0xe8, 0xef, 0xc4,
		0xb7, 0xef, 0x1c, 0xcb, 0x12, 0x62, 0x55, 0xd1,
		0x96, 0x04, 0x7d, 0xfe, 0xdf, 0x17, 0xa0, 0xa9,
	};
	uint8_t in[4] = {'t', 'e', 's', 't'};

	(void) state;
	test_sha_buf(SHA384, SHA384_LEN, in, sizeof(in), refsum);
}

void test_sha384_len_128(void **state)
{
	const uint8_t refsum[SHA384_LEN] = {
		0x9b, 0x56, 0x95, 0xea, 0x34, 0x62, 0xa8, 0xbd,
		0x7f, 0x77, 0x4c, 0xc9, 0x8e, 0xd7, 0x25, 0xfa,
		0xbd, 0x42, 0x5f, 0x6b, 0x39, 0x2d, 0xe7, 0xbf,
		0xbb, 0x45, 0x53, 0x64, 0x6a, 0x81, 0xd1, 0x58,
		0x7c, 0xc9, 0x0a, 0x05, 0xb5, 0x29, 0xa4, 0x94,
		0x43, 0xf5, 0xf6, 0xf0, 0xf2, 0xbb, 0x3b, 0xf9,
	};
	uint8_t in[128];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(in); ++i)
		in[i] = '0' + (i % 10);
	
	test_sha_buf(SHA384, SHA384_LEN, in, sizeof(in), refsum);
}

void test_sha512_len_0(void **state)
{
	const uint8_t refsum[SHA512_LEN] = {
		0xcf, 0x83, 0xe1, 0x35, 0x7e, 0xef, 0xb8, 0xbd,
		0xf1, 0x54, 0x28, 0x50, 0xd6, 0x6d, 0x80, 0x07,
		0xd6, 0x20, 0xe4, 0x05, 0x0b, 0x57, 0x15, 0xdc,
		0x83, 0xf4, 0xa9, 0x21, 0xd3, 0x6c, 0xe9, 0xce,
		0x47, 0xd0, 0xd1, 0x3c, 0x5d, 0x85, 0xf2, 0xb0,
		0xff, 0x83, 0x18, 0xd2, 0x87, 0x7e, 0xec, 0x2f,
		0x63, 0xb9, 0x31, 0xbd, 0x47, 0x41, 0x7a, 0x81,
		0xa5, 0x38, 0x32, 0x7a, 0xf9, 0x27, 0xda, 0x3e,
	};
	uint8_t *in = NULL;

	(void) state;
	test_sha_buf(SHA512, SHA512_LEN, in, 0, refsum);
}

void test_sha512_len_4(void **state)
{
	const uint8_t refsum[SHA512_LEN] = {
		0xee, 0x26, 0xb0, 0xdd, 0x4a, 0xf7, 0xe7, 0x49,
		0xaa, 0x1a, 0x8e, 0xe3, 0xc1, 0x0a, 0xe9, 0x92,
		0x3f, 0x61, 0x89, 0x80, 0x77, 0x2e, 0x47, 0x3f,
		0x88, 0x19, 0xa5, 0xd4, 0x94, 0x0e, 0x0d, 0xb2,
		0x7a, 0xc1, 0x85, 0xf8, 0xa0, 0xe1, 0xd5, 0xf8,
		0x4f, 0x88, 0xbc, 0x88, 0x7f, 0xd6, 0x7b, 0x14,
		0x37, 0x32, 0xc3, 0x04, 0xcc, 0x5f, 0xa9, 0xad,
		0x8e, 0x6f, 0x57, 0xf5, 0x00, 0x28, 0xa8, 0xff,
	};
	uint8_t in[4] = {'t', 'e', 's', 't'};

	(void) state;
	test_sha_buf(SHA512, SHA512_LEN, in, sizeof(in), refsum);
}

void test_sha512_len_128(void **state)
{
	const uint8_t refsum[SHA512_LEN] = {
		0xb6, 0xcb, 0x70, 0x0e, 0xd6, 0xe0, 0xea, 0xf4,
		0xb1, 0x79, 0xb5, 0x90, 0x34, 0x9a, 0xfc, 0x8e,
		0x96, 0x80, 0x41, 0x64, 0xb5, 0xef, 0xc3, 0x6a,
		0xf3, 0xee, 0x8a, 0x35, 0xf4, 0xb8, 0x63, 0x17,
		0x8e, 0x9a, 0x08, 0x9b, 0xdd, 0x10, 0x45, 0x33,
		0x30, 0xa5, 0x6b, 0xa7, 0x7b, 0x7d, 0x83, 0x41,
		0x9d, 0xb0, 0x48, 0xe2, 0x94, 0xbe, 0x23, 0x37,
		0x32, 0x08, 0xef, 0xf4, 0x48, 0xe6, 0xc2, 0xda,
	};
	uint8_t in[128];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(in); ++i)
		in[i] = '0' + (i % 10);
	
	test_sha_buf(SHA512, SHA512_LEN, in, sizeof(in), refsum);
}


/* sha256_extend() tests.
 */

static void sha256_extend_ref(uint32_t *w)
{
	unsigned int i;

	for (i = 16; i < 64; ++i) {
		uint32_t s0;
		uint32_t s1;

		s0 = rr32(w[i-15], 7) ^ rr32(w[i-15], 18) ^ (w[i-15] >> 3);
		s1 = rr32(w[i-2], 17) ^ rr32(w[i-2], 19) ^ (w[i-2] >> 10);
		w[i] = w[i-16] + s0 + w[i-7] + s1;
	}
}

void test_sha256_extend_zeros(void **state)
{
	uint32_t w[64];
	uint32_t wref[64];

	(void) state;

	memset(w, 0, sizeof(w));
	memset(wref, 0, sizeof(wref));

	sha256_extend_ref(wref);
	sha256_extend(w);

	assert_memory_equal(w, wref, sizeof(w));
}

void test_sha256_extend(void **state)
{
	uint32_t w[64];
	uint32_t wref[64];
	unsigned int i;

	(void) state;
	
	for (i = 0; i < sizeof(w) / sizeof(w[0]); ++i)
		wref[i] = w[i] = i | ((i ^ 0xFF) << 22);

	memset(w, 0, sizeof(w));
	memset(wref, 0, sizeof(wref));

	sha256_extend_ref(wref);
	sha256_extend(w);

	assert_memory_equal(w, wref, sizeof(w));
}


/* sha256_compress() tests.
 */

static void sha256_compress_ref(const uint32_t *w, uint32_t *vars)
{
	const uint32_t k[] = {
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
		0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
		0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
		0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
		0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
		0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
		0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
		0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
		0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	};

	unsigned int i;

	for (i = 0; i < 64; ++i) {
		uint32_t S0;
		uint32_t S1;
		uint32_t ch;
		uint32_t maj;
		uint32_t temp1;
		uint32_t temp2;
		unsigned int j;

		S1 = rr32(vars[4], 6) ^ rr32(vars[4], 11) ^ rr32(vars[4], 25);
		ch = (vars[4] & vars[5]) ^ ((~vars[4]) & vars[6]);
		temp1 = vars[7] + S1 + ch + k[i] + w[i];
		S0 = rr32(vars[0], 2) ^ rr32(vars[0], 13) ^ rr32(vars[0], 22);
		maj = (vars[0] & vars[1]) ^ (vars[0] & vars[2])
			^ (vars[1] & vars[2]);
		temp2 = S0 + maj;

		for (j = 7; j > 0; j--)
			vars[j] = vars[j-1];
		vars[4] += temp1;
		vars[0] = temp1 + temp2;
	}
}

void test_sha256_compress_zeros(void **state)
{
	uint32_t varsref[8];
	uint32_t vars[8];
	uint32_t w[64];

	(void) state;

	memset(vars, 0, sizeof(vars));
	memset(varsref, 0, sizeof(varsref));
	memset(w, 0, sizeof(w));

	sha256_compress_ref(w, varsref);
	sha256_compress(w, vars);

	assert_memory_equal(vars, varsref, sizeof(vars));
}

void test_sha256_compress(void **state)
{
	uint32_t varsref[8];
	uint32_t vars[8];
	uint32_t w[64];
	uint32_t i;

	(void) state;

	for (i = 0; i < sizeof(vars) / sizeof(vars[0]); ++i)
		varsref[i] = vars[i] = (i ^ 0xFF) | (i << 22);
	
	for (i = 0; i < sizeof(w) / sizeof(w[0]); ++i)
		w[i] = i | ((i ^ 0xFF) << 22);

	sha256_compress_ref(w, varsref);
	sha256_compress(w, vars);

	assert_memory_equal(vars, varsref, sizeof(vars));
}

/* sha256_extend() tests.
 */

static void sha512_extend_ref(uint64_t *w)
{
	unsigned int i;

	for (i = 16; i < 80; ++i) {
		uint64_t s0;
		uint64_t s1;

		s0 = rr64(w[i-15], 1) ^ rr64(w[i-15], 8) ^ (w[i-15] >> 7);
		s1 = rr64(w[i-2], 19) ^ rr64(w[i-2], 61) ^ (w[i-2] >> 6);
		w[i] = w[i-16] + s0 + w[i-7] + s1;
	}
}

void test_sha512_extend_zeros(void **state)
{
	uint64_t w[80];
	uint64_t wref[80];

	(void) state;

	memset(w, 0, sizeof(w));
	memset(wref, 0, sizeof(wref));

	sha512_extend_ref(wref);
	sha512_extend(w);

	assert_memory_equal(w, wref, sizeof(w));
}

void test_sha512_extend(void **state)
{
	uint64_t w[80];
	uint64_t wref[80];
	uint64_t i;

	(void) state;
	
	for (i = 0; i < sizeof(w) / sizeof(w[0]); ++i)
		wref[i] = w[i] = i | ((i ^ 0xFF) << 54);

	memset(w, 0, sizeof(w));
	memset(wref, 0, sizeof(wref));

	sha512_extend_ref(wref);
	sha512_extend(w);

	assert_memory_equal(w, wref, sizeof(w));
}


/* sha512_compress() tests.
 */

static void sha512_compress_ref(const uint64_t *w, uint64_t *vars)
{
	const uint64_t k[] = {
		0x428a2f98d728ae22, 0x7137449123ef65cd,
		0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
		0x3956c25bf348b538, 0x59f111f1b605d019,
		0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
		0xd807aa98a3030242, 0x12835b0145706fbe,
		0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
		0x72be5d74f27b896f, 0x80deb1fe3b1696b1,
		0x9bdc06a725c71235, 0xc19bf174cf692694,
		0xe49b69c19ef14ad2, 0xefbe4786384f25e3,
		0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
		0x2de92c6f592b0275, 0x4a7484aa6ea6e483,
		0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
		0x983e5152ee66dfab, 0xa831c66d2db43210,
		0xb00327c898fb213f, 0xbf597fc7beef0ee4,
		0xc6e00bf33da88fc2, 0xd5a79147930aa725,
		0x06ca6351e003826f, 0x142929670a0e6e70,
		0x27b70a8546d22ffc, 0x2e1b21385c26c926,
		0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
		0x650a73548baf63de, 0x766a0abb3c77b2a8,
		0x81c2c92e47edaee6, 0x92722c851482353b,
		0xa2bfe8a14cf10364, 0xa81a664bbc423001,
		0xc24b8b70d0f89791, 0xc76c51a30654be30,
		0xd192e819d6ef5218, 0xd69906245565a910,
		0xf40e35855771202a, 0x106aa07032bbd1b8,
		0x19a4c116b8d2d0c8, 0x1e376c085141ab53,
		0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
		0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb,
		0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
		0x748f82ee5defb2fc, 0x78a5636f43172f60,
		0x84c87814a1f0ab72, 0x8cc702081a6439ec,
		0x90befffa23631e28, 0xa4506cebde82bde9,
		0xbef9a3f7b2c67915, 0xc67178f2e372532b,
		0xca273eceea26619c, 0xd186b8c721c0c207,
		0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
		0x06f067aa72176fba, 0x0a637dc5a2c898a6,
		0x113f9804bef90dae, 0x1b710b35131c471b,
		0x28db77f523047d84, 0x32caab7b40c72493,
		0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
		0x4cc5d4becb3e42b6, 0x597f299cfc657e2a,
		0x5fcb6fab3ad6faec, 0x6c44198c4a475817
	};

	unsigned int i;

	for (i = 0; i < 80; ++i) {
		uint64_t S0;
		uint64_t S1;
		uint64_t ch;
		uint64_t maj;
		uint64_t temp1;
		uint64_t temp2;

		S1 = rr64(vars[4], 14) ^ rr64(vars[4], 18) ^ rr64(vars[4], 41);
		ch = (vars[4] & vars[5]) ^ ((~vars[4]) & vars[6]);
		temp1 = vars[7] + S1 + ch + k[i] + w[i];
		S0 = rr64(vars[0], 28) ^ rr64(vars[0], 34) ^ rr64(vars[0], 39);
		maj = (vars[0] & vars[1]) ^ (vars[0] & vars[2])
			^ (vars[1] & vars[2]);
		temp2 = S0 + maj;

		memmove(vars + 1, vars, 7 * sizeof(vars[0]));
		vars[4] += temp1;
		vars[0] = temp1 + temp2;
	}
}

void test_sha512_compress_zeros(void **state)
{
	uint64_t varsref[8];
	uint64_t vars[8];
	uint64_t w[80];

	(void) state;

	memset(vars, 0, sizeof(vars));
	memset(varsref, 0, sizeof(varsref));
	memset(w, 0, sizeof(w));

	sha512_compress_ref(w, varsref);
	sha512_compress(w, vars);

	assert_memory_equal(vars, varsref, sizeof(vars));
}

void test_sha512_compress(void **state)
{
	uint64_t varsref[8];
	uint64_t vars[8];
	uint64_t w[80];
	uint64_t i;

	(void) state;

	for (i = 0; i < sizeof(vars) / sizeof(vars[0]); ++i)
		varsref[i] = vars[i] = (i ^ 0xFF) | (i << 54);
	
	for (i = 0; i < sizeof(w) / sizeof(w[0]); ++i)
		w[i] = i | ((i ^ 0xFF) << 54);

	sha512_compress_ref(w, varsref);
	sha512_compress(w, vars);

	assert_memory_equal(vars, varsref, sizeof(vars));
}

/* Component test.
 */

const struct CMUnitTest sha2_tests[] = {
	/* sha2() */
	cmocka_unit_test(test_sha256_len_0),
	cmocka_unit_test(test_sha256_len_4),
	cmocka_unit_test(test_sha256_len_128),
	cmocka_unit_test(test_sha224_len_0),
	cmocka_unit_test(test_sha224_len_4),
	cmocka_unit_test(test_sha224_len_128),
	cmocka_unit_test(test_sha384_len_0),
	cmocka_unit_test(test_sha384_len_4),
	cmocka_unit_test(test_sha384_len_128),
	cmocka_unit_test(test_sha512_len_0),
	cmocka_unit_test(test_sha512_len_4),
	cmocka_unit_test(test_sha512_len_128),
	/* sha256_extend() */
	cmocka_unit_test(test_sha256_extend_zeros),
	cmocka_unit_test(test_sha256_extend),
	/* sha256_compress() */
	cmocka_unit_test(test_sha256_compress_zeros),
	cmocka_unit_test(test_sha256_compress),
	/* sha512_extend() */
	cmocka_unit_test(test_sha512_extend_zeros),
	cmocka_unit_test(test_sha512_extend),
	/* sha512_compress() */
	cmocka_unit_test(test_sha512_compress_zeros),
	cmocka_unit_test(test_sha512_compress),
};

int main(int argc, char *argv[])
{
	if ((argc == 2) && (!strcmp(argv[1], "--xml"))) {
		cmocka_set_message_output(CM_OUTPUT_XML);
	} else if (argc != 1) {
		fprintf(stderr, "Usage: %s [--xml]\n", argv[0]);
		return 2;
	}

	cmocka_run_group_tests(sha2_tests, NULL, NULL);
	
	return EXIT_SUCCESS;
}

