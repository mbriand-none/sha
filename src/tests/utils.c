#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cmocka.h>

#include "../utils.h"

/* rr32() tests.
 */

void test_rr32_zeros(void **state)
{
	(void) state;

	assert_true(rr32(0, 2) == 0);
}

void test_rr32_ones(void **state)
{
	(void) state;

	assert_true(rr32(0xFFFFFFFF, 2) == 0xFFFFFFFF);
}

void test_rr32_rightmost_one(void **state)
{
	(void) state;

	assert_true(rr32(1, 2) == 0x40000000);
}

void test_rr32_leftmost_one(void **state)
{
	(void) state;

	assert_true(rr32(0x80000000, 2) == 0x20000000);
}

void test_rr32_generic_value(void **state)
{
	(void) state;

	assert_true(rr32(0x66f73A82, 10) == 0xA099bdce);
}

void test_rr32_shift_higher_than_32(void **state)
{
	(void) state;

	assert_true(rr32(1, 37) == rr32(1, 5));
	assert_true(rr32(1, 37) == 0x08000000);
}

/* lr32() tests.
 */

void test_lr32_zeros(void **state)
{
	(void) state;

	assert_true(lr32(0, 2) == 0);
}

void test_lr32_ones(void **state)
{
	(void) state;

	assert_true(lr32(0xFFFFFFFF, 2) == 0xFFFFFFFF);
}

void test_lr32_rightmost_one(void **state)
{
	(void) state;

	assert_true(lr32(1, 2) == 4);
}

void test_lr32_leftmost_one(void **state)
{
	(void) state;

	assert_true(lr32(0x80000000, 2) == 2);
}

void test_lr32_generic_value(void **state)
{
	(void) state;

	assert_true(lr32(0xA099bdce, 10) == 0x66f73A82);
}

void test_lr32_shift_higher_than_32(void **state)
{
	(void) state;

	assert_true(lr32(0x80000000, 37) == lr32(0x80000000, 5));
	assert_true(lr32(0x80000000, 37) == 0x10);
}

/* rr64() tests.
 */

void test_rr64_zeros(void **state)
{
	(void) state;

	assert_true(rr64(0, 2) == 0);
}

void test_rr64_ones(void **state)
{
	(void) state;

	assert_true(rr64(0xFFFFFFFFFFFFFFFF, 2) == 0xFFFFFFFFFFFFFFFF);
}

void test_rr64_rightmost_one(void **state)
{
	(void) state;

	assert_true(rr64(1, 2) == 0x4000000000000000);
}

void test_rr64_leftmost_one(void **state)
{
	(void) state;

	assert_true(rr64(0x8000000000000000, 2) == 0x2000000000000000);
}

void test_rr64_generic_value(void **state)
{
	(void) state;

	assert_true(rr64(0x2298f041a50878af, 10) == 0x2bc8a63c1069421e);
}

void test_rr64_shift_higher_than_64(void **state)
{
	(void) state;

	assert_true(rr64(1, 73) == rr64(1, 9));
	assert_true(rr64(1, 73) == 0x80000000000000);
}

/* Component test.
 */

const struct CMUnitTest input_tests[] = {
	/* rr32() */
	cmocka_unit_test(test_rr32_zeros),
	cmocka_unit_test(test_rr32_ones),
	cmocka_unit_test(test_rr32_rightmost_one),
	cmocka_unit_test(test_rr32_leftmost_one),
	cmocka_unit_test(test_rr32_generic_value),
	cmocka_unit_test(test_rr32_shift_higher_than_32),
	/* lr32() */
	cmocka_unit_test(test_lr32_zeros),
	cmocka_unit_test(test_lr32_ones),
	cmocka_unit_test(test_lr32_rightmost_one),
	cmocka_unit_test(test_lr32_leftmost_one),
	cmocka_unit_test(test_lr32_generic_value),
	cmocka_unit_test(test_lr32_shift_higher_than_32),
	/* rr64() */
	cmocka_unit_test(test_rr64_zeros),
	cmocka_unit_test(test_rr64_ones),
	cmocka_unit_test(test_rr64_rightmost_one),
	cmocka_unit_test(test_rr64_leftmost_one),
	cmocka_unit_test(test_rr64_generic_value),
	cmocka_unit_test(test_rr64_shift_higher_than_64),
};

int main(int argc, char *argv[])
{
	if ((argc == 2) && (!strcmp(argv[1], "--xml"))) {
		cmocka_set_message_output(CM_OUTPUT_XML);
	} else if (argc != 1) {
		fprintf(stderr, "Usage: %s [--xml]\n", argv[0]);
		return 2;
	}

	cmocka_run_group_tests(input_tests, NULL, NULL);
	
	return EXIT_SUCCESS;
}

