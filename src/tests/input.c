#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmocka.h>

#include "../input.c"

/* bufinput_read() tests.
 */

void test_bufinput_read_len_in(const size_t blen, const size_t len)
{
	struct bufinput_ctx bufctx;
	uint8_t in[len];
	uint8_t out[len];
	uint8_t *outp = out;
	int ret;

	memset(in, 0, len);

	bufinput_init(&bufctx, in, len);

	while (outp <= out + sizeof(out)) {
		assert_true(outp <= out + sizeof(out));

		ret = bufinput_read(outp, blen, &bufctx);
		assert_return_code(ret, 0);
		outp += ret;
		
		if ((unsigned int) ret < blen) {
			ret = bufinput_read(outp, blen, &bufctx);
			assert_int_equal(ret, 0);
			break;
		}
		assert_int_equal(ret, blen);
	}

	assert_ptr_equal(outp, out + len);

	assert_memory_equal(in, out, len);
}

void test_bufinput_read_len_512_in_0(void **state)
{
	(void) state;

	test_bufinput_read_len_in(512 / 8, 0);
}

void test_bufinput_read_len_512_in_1(void **state)
{
	(void) state;

	test_bufinput_read_len_in(512 / 8, 1);
}

void test_bufinput_read_len_512_in_63(void **state)
{
	(void) state;

	test_bufinput_read_len_in(512 / 8, 63);
}

void test_bufinput_read_len_512_in_64(void **state)
{
	(void) state;

	test_bufinput_read_len_in(512 / 8, 64);
}

void test_bufinput_read_len_512_in_65(void **state)
{
	(void) state;

	test_bufinput_read_len_in(512 / 8, 65);
}

void test_bufinput_read_len_512_in_0_to_256(void **state)
{
	unsigned int i;

	(void) state;

	for (i = 0; i <= 256; ++i)
		test_bufinput_read_len_in(512 / 8, i);
}

/* getblock() tests.
 */
void test_get_block_len_in_buf(const size_t blen, const size_t len,
			       const uint8_t *in)
{
	struct bufinput_ctx bufctx;
	struct get_block_ctx bctx;
	uint8_t out[len + 2 * blen];
	uint8_t *outp = out;
	uint64_t len64;
	unsigned int k;
	unsigned int i;
	int ret;

	bufinput_init(&bufctx, in, len);
	get_block_init(&bctx);

	while (outp <= out + sizeof(out)) {
		assert_true(outp <= out + sizeof(out));

		ret = get_block(outp, blen, &bctx, bufinput_read, &bufctx);
		if (ret == 0)
			break;
		assert_int_equal(ret, blen);

		outp += ret;
	}

	assert_memory_equal(in, out, len);
	assert_int_equal(out[len], 0x80);
	
	k = (2 * blen - (len % blen) - 1 - sizeof(len64)) % blen;
	for (i = 0; i < k; ++i)
		assert_int_equal(out[len + 1 + i], 0);
	
	len64 = htobe64(len * 8);
	assert_memory_equal(out + len + 1 + k, &len64, sizeof(uint64_t));
}

void test_get_block_len_in_0(const size_t blen, const size_t len)
{
	uint8_t in[len];

	memset(in, 0, len);
	test_get_block_len_in_buf(blen, len, in);
}

void test_get_block_len_in_counter(const size_t blen, const size_t len)
{
	uint8_t in[len];
	unsigned int i;

	for (i = 0; i < len; ++i)
		in[i] = i % 256;

	test_get_block_len_in_buf(blen, len, in);
}

void test_get_block_len_512_in_0(void **state)
{
	(void) state;

	test_get_block_len_in_0(512 / 8, 0);
}

void test_get_block_len_512_in_1(void **state)
{
	(void) state;

	test_get_block_len_in_0(512 / 8, 1);
}

void test_get_block_len_512_in_63(void **state)
{
	(void) state;

	test_get_block_len_in_0(512 / 8, 63);
}

void test_get_block_len_512_in_64(void **state)
{
	(void) state;

	test_get_block_len_in_0(512 / 8, 64);
}

void test_get_block_len_512_in_65(void **state)
{
	(void) state;

	test_get_block_len_in_0(512 / 8, 65);
}

void test_get_block_len_512_in_0_to_256(void **state)
{
	unsigned int i;

	(void) state;

	for (i = 0; i <= 256; ++i)
		test_get_block_len_in_0(512 / 8, i);
}

void test_get_block_len_512_in_0_to_256_counter(void **state)
{
	unsigned int i;

	(void) state;

	for (i = 0; i <= 256; ++i)
		test_get_block_len_in_counter(512 / 8, i);
}

void test_get_block_len_1024_in_0(void **state)
{
	(void) state;

	test_get_block_len_in_0(1024 / 8, 0);
}

void test_get_block_len_1024_in_1(void **state)
{
	(void) state;

	test_get_block_len_in_0(1024 / 8, 1);
}

void test_get_block_len_1024_in_63(void **state)
{
	(void) state;

	test_get_block_len_in_0(1024 / 8, 63);
}

void test_get_block_len_1024_in_64(void **state)
{
	(void) state;

	test_get_block_len_in_0(1024 / 8, 64);
}

void test_get_block_len_1024_in_65(void **state)
{
	(void) state;

	test_get_block_len_in_0(1024 / 8, 65);
}

void test_get_block_len_1024_in_0_to_256(void **state)
{
	unsigned int i;

	(void) state;

	for (i = 0; i <= 256; ++i)
		test_get_block_len_in_0(1024 / 8, i);
}

void test_get_block_len_1024_in_0_to_256_counter(void **state)
{
	unsigned int i;

	(void) state;

	for (i = 0; i <= 256; ++i)
		test_get_block_len_in_counter(1024 / 8, i);
}

/* Component test.
 */

const struct CMUnitTest input_tests[] = {
	/* bufinput_read() */
	cmocka_unit_test(test_bufinput_read_len_512_in_0),
	cmocka_unit_test(test_bufinput_read_len_512_in_1),
	cmocka_unit_test(test_bufinput_read_len_512_in_63),
	cmocka_unit_test(test_bufinput_read_len_512_in_64),
	cmocka_unit_test(test_bufinput_read_len_512_in_65),
	cmocka_unit_test(test_bufinput_read_len_512_in_0_to_256),
	/* get_block() */
	cmocka_unit_test(test_get_block_len_512_in_0),
	cmocka_unit_test(test_get_block_len_512_in_1),
	cmocka_unit_test(test_get_block_len_512_in_63),
	cmocka_unit_test(test_get_block_len_512_in_64),
	cmocka_unit_test(test_get_block_len_512_in_65),
	cmocka_unit_test(test_get_block_len_512_in_0_to_256),
	cmocka_unit_test(test_get_block_len_512_in_0_to_256_counter),
	cmocka_unit_test(test_get_block_len_1024_in_0),
	cmocka_unit_test(test_get_block_len_1024_in_1),
	cmocka_unit_test(test_get_block_len_1024_in_63),
	cmocka_unit_test(test_get_block_len_1024_in_64),
	cmocka_unit_test(test_get_block_len_1024_in_65),
	cmocka_unit_test(test_get_block_len_1024_in_0_to_256),
	cmocka_unit_test(test_get_block_len_1024_in_0_to_256_counter),
};

int main(int argc, char *argv[])
{
	if ((argc == 2) && (!strcmp(argv[1], "--xml"))) {
		cmocka_set_message_output(CM_OUTPUT_XML);
	} else if (argc != 1) {
		fprintf(stderr, "Usage: %s [--xml]\n", argv[0]);
		return 2;
	}

	cmocka_run_group_tests(input_tests, NULL, NULL);
	
	return EXIT_SUCCESS;
}

