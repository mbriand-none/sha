#include <endian.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "input.h"
#include "utils.h"
#include "sha1.h"


static void sha1_extend(uint32_t *w)
{
	unsigned int i;

	for (i = 16; i < 80; ++i) {
		w[i] = lr32(w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16], 1);
	}
}

static void sha1_compress(const uint32_t *w, uint32_t *vars)
{
	unsigned int i;

	for (i = 0; i < 80; ++i) {
		uint32_t f;
		uint32_t k;
		uint32_t temp;

		if (i < 20) {
			f = (vars[1] & vars[2]) | ((~vars[1]) & vars[3]);
			k = 0x5A827999;
		} else if (i < 40) {
			f = vars[1] ^ vars[2] ^ vars[3];
			k = 0x6ED9EBA1;
		} else if (i < 60) {
			f = (vars[1] & vars[2]) | (vars[1] & vars[3]) | (vars[2] & vars[3]);
			k = 0x8F1BBCDC;
		} else {
			f = vars[1] ^ vars[2] ^ vars[3];
			k = 0xCA62C1D6;
		}

		temp = lr32(vars[0], 5) + f + vars[4] + k + w[i];
		vars[4] = vars[3];
		vars[3] = vars[2];
		vars[2] = lr32(vars[1], 30);
		vars[1] = vars[0];
		vars[0] = temp;
	}
}

static uint32_t sha1_h[5] = {
	0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0,
};

#define SHA1_BLEN	(512 / 8)

static int sha1(unsigned char *sum,
	   int (*input_read)(unsigned char *, size_t, void *),
	   void *input_ctx)
{
	uint32_t h[5];
	struct get_block_ctx bctx;
	unsigned int i;

	memcpy(h, sha1_h, sizeof(h));

	get_block_init(&bctx);

	while (1) {
		uint8_t block[SHA1_BLEN];
		uint32_t vars[5];
		uint32_t w[80];
		int ret;

		ret = get_block(block, SHA1_BLEN, &bctx, input_read, input_ctx);
		if (ret < 0) {
			return ret;
		} else if (ret == 0) {
			break;
		} else if (ret != SHA1_BLEN) {
			return -EIO;
		}

		memcpy(vars, h, sizeof(vars));

		for (i = 0; i < 16; ++i) {
			w[i] = (((uint32_t) block[4*i+0] << 24) +
				((uint32_t) block[4*i+1] << 16) +
				((uint32_t) block[4*i+2] <<  8) +
				((uint32_t) block[4*i+3] <<  0));
		}

		sha1_extend(w);
		sha1_compress(w, vars);

		for (i = 0; i < 8; ++i)
			h[i] += vars[i];
	}

	for (i = 0; i < SHA1_LEN / sizeof(h[0]); ++i) {
		uint32_t s;

		s = htobe32(h[i]);
		memcpy(sum + i * sizeof(h[0]), &s, sizeof(h[0]));
	}

	return 0;
}

int sha1_fd(const int fd, unsigned char *sum)
{
	struct fdinput_ctx fdctx;

	fdinput_init(&fdctx, fd);

	return sha1(sum, fdinput_read, &fdctx);
}

int sha1_buf(const uint8_t *buf, size_t len, unsigned char *sum)
{
	struct bufinput_ctx bufctx;

	bufinput_init(&bufctx, buf, len);

	return sha1(sum, bufinput_read, &bufctx);
}

