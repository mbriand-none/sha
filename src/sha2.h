#ifndef __SHA2_H__
#define __SHA2_H__

#include <stdint.h>
#include <unistd.h>

#include "sha.h"

#define SHA224_LEN	(224 / 8)
#define SHA256_LEN	(256 / 8)
#define SHA384_LEN	(384 / 8)
#define SHA512_LEN	(512 / 8)

int sha2_buf(enum sha_alg alg, const uint8_t *buf, size_t len,
	     unsigned char *sum);
int sha2_fd(enum sha_alg alg, const int fd, unsigned char *sum);

#endif
