#!/bin/bash

OUT=$(dirname $0)/../out/

if [[ `git status --porcelain -uno` ]]; then
	echo "Uncommitted modifications in git: aborting"
	exit 1
fi

version=$(git describe --always)
branch=$(git show -s --pretty=%D HEAD | tr -s ', ' '\n' | sed -ne 's/^origin\///p' | tail -n1)
date=$(date "+%Y%m%d-%H%M%S")

rm -rf ${OUT}/stats_git/
git clone git@bitbucket.org:mbriand-none/sha_stats.git ${OUT}/stats_git/
pushd ${OUT}/stats_git/

# If the branch does not exist, we will remain on master: this is fine for now
git checkout origin/${branch}
git branch -D ${branch}
git checkout -b ${branch}

popd
mkdir -p ${OUT}/stats_git/data
for file in ${OUT}/tests/stats/*; do
	echo $file
	echo -e "${date}\t\"${version}\"\t$(cat ${file})" \
		>> ${OUT}/stats_git/data/$(basename ${file}).csv
done

pushd ${OUT}/stats_git/
git add data
git config user.email "mathieu.briand@hyprua.org"
git config user.name "Continuous integration"
git commit -m "Add stats for ${version}"
git push origin ${branch}
popd

